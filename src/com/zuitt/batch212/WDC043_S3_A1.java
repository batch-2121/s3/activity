package com.zuitt.batch212;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int in = 1;

        try {
            System.out.println("Input an integer whose factorial will be computed");
            in = input.nextInt();
            System.out.println(in);
            int result = 1;

            for(int i=in; in>0; in--){
                result *= in;
            }
            System.out.println("The Factorial of "+in+" is "+result);

            while (in>0){
                result *= in;
                in--;
            }
            System.out.println("The Factorial of "+in+" is "+result);
        }
        catch (InputMismatchException e){
            System.out.println("Input is not an number");
        }



    }
}
